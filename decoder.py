#!bin/python3

ctf_string = "ppaHaH ykkun !harreMhC ytsir!sampaH N ypY we!raevaH  a egalfFTC d10{fbf6f6257784459adb3abbcccf22 .}6 uoYesed evr!!ti"
sentence = ""
index = 1
one =  1
four = 4
#containst the list of each element in ctf_strinf
list_char=[]
#holds list of reversed/decoded elements from ctf_string 
container = []


def to_list(string_data, list_needed):
    """Create a list of char's"""
    for each in string_data:
        list_needed.append(each)


def remainder():
    """Returns the remainder when lenght of list is divided by 4"""
    return len(list_char)/four


def decode_string(index, empty_list):
    """decode the given message by reversing every 4-characters"""
    r = remainder()
    while index <= r:
        start = four*(index-one)
        end = four*index
        [empty_list.append(x) for x in reversed(list_char[start:end])]
        index += one


def decoded_message(string_to_decode, working_list, index):
    """returns the decoded string"""
    to_list(string_to_decode, working_list) 
    decode_string(index,container)
    sentence = ""
    for chars in container:
        sentence += chars
    return sentence

print(decoded_message(ctf_string, list_char, index))
